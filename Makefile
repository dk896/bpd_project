up: docker-up
down: docker-down
restart: docker-down docker-up
init-l: docker-down-clear investigation-clear docker-pull docker-build docker-up investigation-init-light rights
init-f: docker-down-clear investigation-clear docker-pull docker-build docker-up investigation-full-init rights
test: investigation-test
test-unit: investigation-test-unit
test-func: investigation-test-func
rights: investigation-cli-rightes investigation-fpm-rightes
diff: investigation-migrations-diff
migrate: investigation-migrations
clear: investigation-cache-clear

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

investigation-init-light: investigation-composer-install investigation-assets-install investigation-wait-db investigation-migrations investigation-ready

investigation-full-init: investigation-composer-install investigation-assets-install investigation-oauth-keys investigation-wait-db investigation-migrations investigation-fixtures investigation-ready

investigation-clear:
	docker run --rm -v ${PWD}/investigation:/app --workdir=/app node:13-alpine rm -f .ready

investigation-ready:
	docker run --rm -v ${PWD}/investigation:/app --workdir=/app node:13-alpine touch .ready

investigation-composer-install:
	docker-compose run --rm investigation-php-cli composer install

investigation-composer-update:
	docker-compose run --rm investigation-php-cli composer update

investigation-assets-install:
	docker-compose run --rm investigation-node yarn install
	docker-compose run --rm investigation-node npm rebuild node-sass

investigation-assets-upgrade:
	docker-compose run --rm investigation-node yarn upgrade

investigation-assets-node-sass:
	docker-compose run --rm investigation-node yarn add --force node-sass@latest

investigation-assets-dev:
	docker-compose run --rm investigation-node yarn run dev

investigation-assets-watch:
	docker-compose run --rm investigation-node-watch

investigation-oauth-keys:
	docker-compose run --rm investigation-php-cli mkdir -p var/oauth
	docker-compose run --rm investigation-php-cli openssl genrsa -out var/oauth/private.key 2048
	docker-compose run --rm investigation-php-cli openssl rsa -in var/oauth/private.key -pubout -out var/oauth/public.key
	docker-compose run --rm investigation-php-cli chmod 644 var/oauth/private.key var/oauth/public.key

investigation-wait-db:
	until docker-compose exec -T investigation-postgres pg_isready --timeout=0 --dbname=app ; do sleep 1 ; done

investigation-migrations:
	docker-compose run --rm investigation-php-cli php bin/console doctrine:migrations:migrate --no-interaction

investigation-migrations-diff:
	docker-compose run --rm investigation-php-cli php bin/console doctrine:migrations:diff --no-interaction

investigation-fixtures:
	docker-compose run --rm investigation-php-cli php bin/console doctrine:fixtures:load --no-interaction

investigation-cli-rightes:
	docker-compose run --rm investigation-php-cli chmod -R 777 /app/var

investigation-fpm-rightes:
	docker-compose run --rm investigation-php-fpm chmod -R 777 /app/var

investigation-test:
	docker-compose run --rm investigation-php-cli php bin/phpunit

investigation-test-unit:
	docker-compose run --rm investigation-php-cli php bin/phpunit --testsuite=unit

investigation-test-func:
	docker-compose run --rm investigation-php-cli php bin/phpunit --testsuite=functional

investigation-cache-clear:
	docker-compose run --rm investigation-php-cli php bin/console cache:clear
