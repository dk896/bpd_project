<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403173029 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE work_investigations_investigation_own (id UUID NOT NULL, investigation_id UUID DEFAULT NULL, date_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_end TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, result VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_43B7E904DC64C387 ON work_investigations_investigation_own (investigation_id)');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_own.id IS \'(DC2Type:work_investigations_investigation_own_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_own.investigation_id IS \'(DC2Type:work_investigations_investigation_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_own.date_start IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_own.date_end IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE work_investigations_own_investigation_managers (investigation_id UUID NOT NULL, manager_id UUID NOT NULL, PRIMARY KEY(investigation_id, manager_id))');
        $this->addSql('CREATE INDEX IDX_C67D3A33DC64C387 ON work_investigations_own_investigation_managers (investigation_id)');
        $this->addSql('CREATE INDEX IDX_C67D3A33783E3463 ON work_investigations_own_investigation_managers (manager_id)');
        $this->addSql('COMMENT ON COLUMN work_investigations_own_investigation_managers.investigation_id IS \'(DC2Type:work_investigations_investigation_own_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_own_investigation_managers.manager_id IS \'(DC2Type:work_investigations_investigation_manager_id)\'');
        $this->addSql('CREATE TABLE work_investigations_investigations (id UUID NOT NULL, date_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_end TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, name VARCHAR(255) NOT NULL, reason VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, result VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigations.id IS \'(DC2Type:work_investigations_investigation_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigations.date_start IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigations.date_end IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE work_investigations__managers (investigation_id UUID NOT NULL, manager_id UUID NOT NULL, PRIMARY KEY(investigation_id, manager_id))');
        $this->addSql('CREATE INDEX IDX_C91775E8DC64C387 ON work_investigations__managers (investigation_id)');
        $this->addSql('CREATE INDEX IDX_C91775E8783E3463 ON work_investigations__managers (manager_id)');
        $this->addSql('COMMENT ON COLUMN work_investigations__managers.investigation_id IS \'(DC2Type:work_investigations_investigation_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations__managers.manager_id IS \'(DC2Type:work_investigations_investigation_manager_id)\'');
        $this->addSql('CREATE TABLE work_investigations_investigation_couгt (id UUID NOT NULL, investigation_id UUID DEFAULT NULL, date_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_end TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, result VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_97B2AECFDC64C387 ON work_investigations_investigation_couгt (investigation_id)');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_couгt.id IS \'(DC2Type:work_investigations_investigation_couгt_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_couгt.investigation_id IS \'(DC2Type:work_investigations_investigation_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_couгt.date_start IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_couгt.date_end IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_couгt.status IS \'(DC2Type:work_investigations_investigation_couгt_status)\'');
        $this->addSql('CREATE TABLE work_investigations_court_investigation_executors (investigation_id UUID NOT NULL, executor_id UUID NOT NULL, PRIMARY KEY(investigation_id, executor_id))');
        $this->addSql('CREATE INDEX IDX_2B4F27F1DC64C387 ON work_investigations_court_investigation_executors (investigation_id)');
        $this->addSql('CREATE INDEX IDX_2B4F27F18ABD09BB ON work_investigations_court_investigation_executors (executor_id)');
        $this->addSql('COMMENT ON COLUMN work_investigations_court_investigation_executors.investigation_id IS \'(DC2Type:work_investigations_investigation_couгt_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_court_investigation_executors.executor_id IS \'(DC2Type:work_investigations_investigation_manager_id)\'');
        $this->addSql('CREATE TABLE work_investigations_investigation_managers (id UUID NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) NOT NULL, name_first VARCHAR(255) NOT NULL, name_middle VARCHAR(255) NOT NULL, name_last VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D01A6CBE7927C74 ON work_investigations_investigation_managers (email)');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_managers.id IS \'(DC2Type:work_investigations_investigation_manager_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_managers.email IS \'(DC2Type:work_investigations_investigation_manager_email)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_managers.phone IS \'(DC2Type:work_investigations_investigation_manager_phone)\'');
        $this->addSql('CREATE TABLE work_investigations_investigation_police (id UUID NOT NULL, investigation_id UUID DEFAULT NULL, date_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_end TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, result VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1EE6E69DDC64C387 ON work_investigations_investigation_police (investigation_id)');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_police.id IS \'(DC2Type:work_investigations_investigation_police_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_police.investigation_id IS \'(DC2Type:work_investigations_investigation_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_police.date_start IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_police.date_end IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_investigation_police.status IS \'(DC2Type:work_investigations_investigation_police_status)\'');
        $this->addSql('CREATE TABLE work_investigations_police_investigation_executors (investigation_id UUID NOT NULL, executor_id UUID NOT NULL, PRIMARY KEY(investigation_id, executor_id))');
        $this->addSql('CREATE INDEX IDX_CD0AEE0EDC64C387 ON work_investigations_police_investigation_executors (investigation_id)');
        $this->addSql('CREATE INDEX IDX_CD0AEE0E8ABD09BB ON work_investigations_police_investigation_executors (executor_id)');
        $this->addSql('COMMENT ON COLUMN work_investigations_police_investigation_executors.investigation_id IS \'(DC2Type:work_investigations_investigation_police_id)\'');
        $this->addSql('COMMENT ON COLUMN work_investigations_police_investigation_executors.executor_id IS \'(DC2Type:work_investigations_investigation_manager_id)\'');
        $this->addSql('CREATE TABLE work_contracts_contract_persons (id UUID NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) NOT NULL, name_first VARCHAR(255) NOT NULL, name_middle VARCHAR(255) NOT NULL, name_last VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A6C647BDE7927C74 ON work_contracts_contract_persons (email)');
        $this->addSql('COMMENT ON COLUMN work_contracts_contract_persons.id IS \'(DC2Type:work_contracts_contract_person_id)\'');
        $this->addSql('COMMENT ON COLUMN work_contracts_contract_persons.email IS \'(DC2Type:work_contracts_contract_person_email)\'');
        $this->addSql('COMMENT ON COLUMN work_contracts_contract_persons.phone IS \'(DC2Type:work_contracts_contract_person_phone)\'');
        $this->addSql('CREATE TABLE work_contracts_contracts (id UUID NOT NULL, person_id UUID NOT NULL, investigation_id UUID DEFAULT NULL, number VARCHAR(255) NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, contract_sum VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1E869FD6217BBB47 ON work_contracts_contracts (person_id)');
        $this->addSql('CREATE INDEX IDX_1E869FD6DC64C387 ON work_contracts_contracts (investigation_id)');
        $this->addSql('COMMENT ON COLUMN work_contracts_contracts.id IS \'(DC2Type:work_contracts_contract_id)\'');
        $this->addSql('COMMENT ON COLUMN work_contracts_contracts.person_id IS \'(DC2Type:work_contracts_contract_person_id)\'');
        $this->addSql('COMMENT ON COLUMN work_contracts_contracts.investigation_id IS \'(DC2Type:work_investigations_investigation_id)\'');
        $this->addSql('COMMENT ON COLUMN work_contracts_contracts.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE work_investigations_investigation_own ADD CONSTRAINT FK_43B7E904DC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_own_investigation_managers ADD CONSTRAINT FK_C67D3A33DC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigation_own (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_own_investigation_managers ADD CONSTRAINT FK_C67D3A33783E3463 FOREIGN KEY (manager_id) REFERENCES work_investigations_investigation_managers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations__managers ADD CONSTRAINT FK_C91775E8DC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations__managers ADD CONSTRAINT FK_C91775E8783E3463 FOREIGN KEY (manager_id) REFERENCES work_investigations_investigation_managers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_investigation_couгt ADD CONSTRAINT FK_97B2AECFDC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_court_investigation_executors ADD CONSTRAINT FK_2B4F27F1DC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigation_couгt (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_court_investigation_executors ADD CONSTRAINT FK_2B4F27F18ABD09BB FOREIGN KEY (executor_id) REFERENCES work_investigations_investigation_managers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_investigation_police ADD CONSTRAINT FK_1EE6E69DDC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_police_investigation_executors ADD CONSTRAINT FK_CD0AEE0EDC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigation_police (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_investigations_police_investigation_executors ADD CONSTRAINT FK_CD0AEE0E8ABD09BB FOREIGN KEY (executor_id) REFERENCES work_investigations_investigation_managers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_contracts_contracts ADD CONSTRAINT FK_1E869FD6217BBB47 FOREIGN KEY (person_id) REFERENCES work_contracts_contract_persons (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_contracts_contracts ADD CONSTRAINT FK_1E869FD6DC64C387 FOREIGN KEY (investigation_id) REFERENCES work_investigations_investigations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE work_investigations_own_investigation_managers DROP CONSTRAINT FK_C67D3A33DC64C387');
        $this->addSql('ALTER TABLE work_investigations_investigation_own DROP CONSTRAINT FK_43B7E904DC64C387');
        $this->addSql('ALTER TABLE work_investigations__managers DROP CONSTRAINT FK_C91775E8DC64C387');
        $this->addSql('ALTER TABLE work_investigations_investigation_couгt DROP CONSTRAINT FK_97B2AECFDC64C387');
        $this->addSql('ALTER TABLE work_investigations_investigation_police DROP CONSTRAINT FK_1EE6E69DDC64C387');
        $this->addSql('ALTER TABLE work_contracts_contracts DROP CONSTRAINT FK_1E869FD6DC64C387');
        $this->addSql('ALTER TABLE work_investigations_court_investigation_executors DROP CONSTRAINT FK_2B4F27F1DC64C387');
        $this->addSql('ALTER TABLE work_investigations_own_investigation_managers DROP CONSTRAINT FK_C67D3A33783E3463');
        $this->addSql('ALTER TABLE work_investigations__managers DROP CONSTRAINT FK_C91775E8783E3463');
        $this->addSql('ALTER TABLE work_investigations_court_investigation_executors DROP CONSTRAINT FK_2B4F27F18ABD09BB');
        $this->addSql('ALTER TABLE work_investigations_police_investigation_executors DROP CONSTRAINT FK_CD0AEE0E8ABD09BB');
        $this->addSql('ALTER TABLE work_investigations_police_investigation_executors DROP CONSTRAINT FK_CD0AEE0EDC64C387');
        $this->addSql('ALTER TABLE work_contracts_contracts DROP CONSTRAINT FK_1E869FD6217BBB47');
        $this->addSql('DROP TABLE work_investigations_investigation_own');
        $this->addSql('DROP TABLE work_investigations_own_investigation_managers');
        $this->addSql('DROP TABLE work_investigations_investigations');
        $this->addSql('DROP TABLE work_investigations__managers');
        $this->addSql('DROP TABLE work_investigations_investigation_couгt');
        $this->addSql('DROP TABLE work_investigations_court_investigation_executors');
        $this->addSql('DROP TABLE work_investigations_investigation_managers');
        $this->addSql('DROP TABLE work_investigations_investigation_police');
        $this->addSql('DROP TABLE work_investigations_police_investigation_executors');
        $this->addSql('DROP TABLE work_contracts_contract_persons');
        $this->addSql('DROP TABLE work_contracts_contracts');
    }
}
