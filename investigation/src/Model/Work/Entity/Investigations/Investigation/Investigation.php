<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Investigations\Investigation;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Model\Work\Entity\Contracts\Contract\Contract;
use App\Model\Work\Entity\Investigations\Investigation\Own\Own;
use App\Model\Work\Entity\Investigations\Investigation\Couгt\Couгt;
use App\Model\Work\Entity\Investigations\Investigation\Police\Police;
use App\Model\Work\Entity\Investigations\Investigation\Manager\Manager;
use App\Model\Work\Entity\Investigations\Investigation\Manager\Id as ManagerId;

/**
 * @ORM\Entity
 * @ORM\Table(
 *  name="work_investigations_investigations"
 * )
 */
class Investigation
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DONE = 'done';

    /**
     * @ORM\Column(type="work_investigations_investigation_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $dateStart;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $reason;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $result;

    /**
     * @var Manager[]|ArrayCollection
     *
     * @ORM\ManyToMany(
     *      targetEntity="App\Model\Work\Entity\Investigations\Investigation\Manager\Manager"
     * )
     * @ORM\JoinTable(
     *  name="work_investigations__managers",
     *  joinColumns={@ORM\JoinColumn(name="investigation_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="manager_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"name.first" = "ASC"})
     */
    private $managers;

    /**
     * @var Contract[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *  targetEntity="App\Model\Work\Entity\Contracts\Contract\Contract",
     *  mappedBy="investigation",
     *  orphanRemoval=true,
     *  cascade={"all"}
     * )
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $contracts;

    /**
     * @var Own[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *  targetEntity="App\Model\Work\Entity\Investigations\Investigation\Own\Own",
     *  mappedBy="investigation",
     *  orphanRemoval=true,
     *  cascade={"persist"}
     * )
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $owns;

    /**
     * @var Police[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *  targetEntity="App\Model\Work\Entity\Investigations\Investigation\Police\Police",
     *  mappedBy="investigation",
     *  orphanRemoval=true,
     *  cascade={"persist"}
     * )
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $polices;

    /**
     * @var Couгt[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *  targetEntity="App\Model\Work\Entity\Investigations\Investigation\Couгt\Couгt",
     *  mappedBy="investigation",
     *  orphanRemoval=true,
     *  cascade={"persist"}
     * )
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $couгts;

    public function __construct(
        Id $id
    ) {
        $this->id = $id;
        $this->dateStart = new \DateTimeImmutable();
        $this->managers = new ArrayCollection();
        $this->contracts = new ArrayCollection();
        $this->owns = new ArrayCollection();
        $this->polices = new ArrayCollection();
        $this->couгts = new ArrayCollection();
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of dateStart
     *
     * @return  \DateTimeImmutable
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Get the value of dateEnd
     *
     * @return  \DateTimeImmutable
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of reason
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the value of result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Get the value of managers
     *
     * @return  Manager[]|ArrayCollection
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * Get the value of contracts
     *
     * @return  Contract[]|ArrayCollection
     */
    public function getContracts()
    {
        return $this->contracts;
    }

    /**
     * Get the value of owns
     *
     * @return  Own[]|ArrayCollection
     */
    public function getOwns()
    {
        return $this->owns;
    }

    /**
     * Get the value of polices
     *
     * @return  Police[]|ArrayCollection
     */
    public function getPolices()
    {
        return $this->polices;
    }

    /**
     * Get the value of couгts
     *
     * @return  Couгt[]|ArrayCollection
     */
    public function getCouгts()
    {
        return $this->couгts;
    }

    public function assignManager(
        Manager $manager
    ): void {
        if ($this->managers->contains($manager)) {
            throw new \DomainException('Given manager is already assigned.');
        }

        $this->managers->add($manager);
    }

    public function revokeManager(
        ManagerId $id
    ): void {
        foreach ($this->managers as $current) {
            if ($current->getId()->isEqual($id)) {
                $this->managers->removeElement($current);

                return;
            }
        }

        throw new \DomainException('Given manager is not assigned.');
    }

    public function hasManager(ManagerId $id): bool
    {
       foreach ($this->managers as $manager) {
            if ($manager->getId()->isEqual($id)) {
                return true;
            }
        }

        return false;
    }
}
