<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Investigations\Investigation\Own;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Model\Work\Entity\Contracts\Contract\Contract;
use App\Model\Work\Entity\Investigations\Investigation\Investigation;
use App\Model\Work\Entity\Investigations\Investigation\Manager\Manager;
use App\Model\Work\Entity\Investigations\Investigation\Manager\Id as ManagerId;

/**
 * @ORM\Entity
 * @ORM\Table(
 *  name="work_investigations_investigation_own"
 * )
 */
class Own
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DONE = 'done';

    /**
     * @ORM\Column(type="work_investigations_investigation_own_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $dateStart;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $dateEnd;

    /**
     * @var Manager[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Model\Work\Entity\Investigations\Investigation\Manager\Manager")
     * @ORM\JoinTable(
     *  name="work_investigations_own_investigation_managers",
     *  joinColumns={@ORM\JoinColumn(name="investigation_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="manager_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"name.first" = "ASC"})
     */
    private $managers;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $result;

    /**
     * @var Investigation
     *
     * @ORM\ManyToOne(
     *  targetEntity="App\Model\Work\Entity\Investigations\Investigation\Investigation",
     *  inversedBy="owns"
     * )
     * @ORM\JoinColumn(
     *  name="investigation_id",
     *  referencedColumnName="id"
     * )
     */
    private $investigation;

    public function __construct(
        Id $id,
        string $name,
        Investigation $investigation
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->status = self::STATUS_ACTIVE;
        $this->dateStart = new \DateTimeImmutable();
        $this->managers = new ArrayCollection();
        $this->investigation = $investigation;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of dateStart
     *
     * @return  \DateTimeImmutable
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Get the value of dateEnd
     *
     * @return  \DateTimeImmutable
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Get managers of own investigation
     *
     * @return  ArrayCollection
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the value of result
     */
    public function getResult()
    {
        return $this->result;
    }

    public function assignManager(
        Manager $manager
    ): void {
        if ($this->managers->contains($manager)) {
            throw new \DomainException('Given manager is already assigned.');
        }

        $this->managers->add($manager);
    }

    public function revokeManager(
        ManagerId $id
    ): void {
        foreach ($this->managers as $current) {
            if ($current->getId()->isEqual($id)) {
                $this->managers->removeElement($current);

                return;
            }
        }

        throw new \DomainException('Given manager is not assigned.');
    }

    public function hasManager(ManagerId $id): bool
    {
       foreach ($this->managers as $manager) {
            if ($manager->getId()->isEqual($id)) {
                return true;
            }
        }

        return false;
    }
}
