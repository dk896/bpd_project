<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Investigations\Investigation\Manager;

use App\Model\EntityNotFoundException;
use Doctrine\ORM\EntityManagerInterface;

class ManagerRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Manager::class);
    }

    /**
     * @param Id $id
     * @return Manager
     */
    public function get(Id $id): Manager
    {
        /** @var Manager $manager */
        if (!$manager = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Manager is not found.');
        }

        return $manager;
    }

    /**
     * @param Email $email
     * @return Manager
     */
    public function getByEmail(Email $email): Manager
    {
        /** @var Manager $manager */
        if (!$manager = $this->repo->findOneBy([
            'email' => $email->getValue(),
        ])) {
            throw new EntityNotFoundException('Manager is not found.');
        }

        return $manager;
    }

    /**
     * @param Email $email
     * @return boolean
     */
    public function hasByEmail(Email $email): bool
    {
        return $this->repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->andWhere('t.email = :email')
            ->setParameter(':email', $email->getValue())
            ->getQuery()
            ->getSingleScalarResult()
        > 0;
    }

    /**
     * @param Manager $manager
     * @return void
     */
    public function add(Manager $manager): void
    {
        $this->em->persist($manager);
    }
}
