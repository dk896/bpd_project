<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Investigations\Investigation\Manager;

use App\Model\Work\Entity\Contracts\Contract\Contract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *  name="work_investigations_investigation_managers",
 *  uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"email"})
 * })
 */
class Manager
{
    /**
     * @ORM\Column(type="work_investigations_investigation_manager_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var Email|null
     * @ORM\Column(type="work_investigations_investigation_manager_email", nullable=true)
     */
    private $email;

    /**
     * @var Phone
     * @ORM\Column(type="work_investigations_investigation_manager_phone")
     */
    private $phone;

    /**
     * @var Name
     * @ORM\Embedded(class="Name")
     */
    private $name;

    public function __construct(
        Id $id,
        Email $email,
        Phone $phone,
        Name $name
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->phone = $phone;
        $this->name = $name;
    }

    public function edit(
        Email $email,
        Phone $phone,
        Name $name
    ): void {
        $this->email = $email;
        $this->phone = $phone;
        $this->name = $name;
    }

    /**
     * Get the value of id object
     *
     * @return  Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Get the value of phone object
     *
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * Get the value of email object
     *
     * @return  Email
     */
    public function getEmail(): ?Email
    {
        return $this->email;
    }

    /**
     * Get the value of name
     *
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }
}
