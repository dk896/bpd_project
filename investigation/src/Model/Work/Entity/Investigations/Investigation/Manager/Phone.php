<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Investigations\Investigation\Manager;

use Webmozart\Assert\Assert;

class Phone
{
    private $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value);

        /* TODO Validate phone number by regexp */

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function isEqual(self $other): bool
    {
        return $this->getValue() === $other->getValue();
    }
}
