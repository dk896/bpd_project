<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Investigations\Investigation\Manager;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class PhoneType extends StringType
{
    public const NAME = 'work_investigations_investigation_manager_phone';

    public function convertToDatabaseValue(
        $value,
        AbstractPlatform $platform
    ) {
        return $value instanceof Phone ? $value->getValue() : $value;
    }

    public function convertToPHPValue(
        $value,
        AbstractPlatform $platform
    ) {
        return !empty($value) ? new Phone($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
