<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Investigations\Investigation\Couгt;

use Webmozart\Assert\Assert;

class Status
{
    public const ACTIVE   = 'active';
    public const REJECTED = 'rejected';
    public const APPEALED = 'appealed';
    public const DONE = 'done';

    private $name;

    public function __construct(string $name)
    {
        Assert::oneOf(
            $name,
            [
                self::ACTIVE,
                self::REJECTED,
                self::APPEALED,
                self::DONE,
            ]
        );

        $this->name = $name;
    }

    public static function active(): self
    {
        return new self(self::ACTIVE);
    }

    public static function rejected(): self
    {
        return new self(self::REJECTED);
    }

    public static function appealed(): self
    {
        return new self(self::APPEALED);
    }

    public static function done(): self
    {
        return new self(self::DONE);
    }

    public function isEqual(self $other): bool
    {
        return $this->getName() === $other->getName();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isRejected(): bool
    {
        return $this->name === self::ACTIVE;
    }

    public function isAppealed(): bool
    {
        return $this->name === self::APPEALED;
    }

    public function isDone(): bool
    {
        return $this->name === self::DONE;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
