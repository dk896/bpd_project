<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Investigations\Investigation\Couгt;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Model\Work\Entity\Contracts\Contract\Contract;
use App\Model\Work\Entity\Investigations\Investigation\Investigation;
use App\Model\Work\Entity\Investigations\Investigation\Manager\Manager;
use App\Model\Work\Entity\Investigations\Investigation\Manager\Id as ExecutorId;

/**
 * @ORM\Entity
 * @ORM\Table(
 *  name="work_investigations_investigation_couгt"
 * )
 */
class Couгt
{
    /**
     * @ORM\Column(type="work_investigations_investigation_couгt_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $dateStart;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $dateEnd;

    /**
     * @var Manager[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Model\Work\Entity\Investigations\Investigation\Manager\Manager")
     * @ORM\JoinTable(
     *  name="work_investigations_court_investigation_executors",
     *  joinColumns={@ORM\JoinColumn(name="investigation_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="executor_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"name.first" = "ASC"})
     */
    private $executors;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="work_investigations_investigation_couгt_status")
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $result;

    /**
     * @var Investigation
     *
     * @ORM\ManyToOne(
     *  targetEntity="App\Model\Work\Entity\Investigations\Investigation\Investigation",
     *  inversedBy="couгts"
     * )
     * @ORM\JoinColumn(
     *  name="investigation_id",
     *  referencedColumnName="id"
     * )
     */
    private $investigation;

    public function __construct(
        Id $id,
        string $name,
        Investigation $investigation
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->status = Status::active();
        $this->dateStart = new \DateTimeImmutable();
        $this->executors = new ArrayCollection();
        $this->investigation = $investigation;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of dateStart
     *
     * @return  \DateTimeImmutable
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Get the value of dateEnd
     *
     * @return  \DateTimeImmutable
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Get executors of investigation
     *
     * @return ArrayCollection
     */
    public function getExecutors()
    {
        return $this->executors;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the value of result
     */
    public function getResult()
    {
        return $this->result;
    }

    public function assignExecutor(
        Manager $executor
    ): void {
        if ($this->executors->contains($executor)) {
            throw new \DomainException('Given executor is already assigned.');
        }

        $this->executors->add($executor);
    }

    public function revokeExecutor(
        ExecutorId $id
    ): void {
        foreach ($this->executors as $current) {
            if ($current->getId()->isEqual($id)) {
                $this->executors->removeElement($current);

                return;
            }
        }

        throw new \DomainException('Given executor is not assigned.');
    }

    public function hasExecutor(ExecutorId $id): bool
    {
       foreach ($this->executors as $executor) {
            if ($executor->getId()->isEqual($id)) {
                return true;
            }
        }

        return false;
    }
}
