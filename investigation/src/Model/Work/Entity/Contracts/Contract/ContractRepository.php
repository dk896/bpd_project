<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Contracts\Contract;

use App\Model\EntityNotFoundException;
use Doctrine\ORM\EntityManagerInterface;

class ContractRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Contract::class);
    }

    public function get(Id $id): Contract
    {
        /** @var Contract $contract */
        if (!$contract = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Contract is not found.');
        }

        return $contract;
    }

    public function add(Contract $contract): void
    {
        $this->em->persist($contract);
    }

    public function remove(Contract $contract): void
    {
        $this->em->remove($contract);
    }
}
