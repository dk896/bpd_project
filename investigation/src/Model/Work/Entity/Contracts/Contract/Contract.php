<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Contracts\Contract;

use Doctrine\ORM\Mapping as ORM;
use App\Model\Work\Entity\Contracts\Contract\Person\Person;
use App\Model\Work\Entity\Investigations\Investigation\Investigation;

/**
 * @ORM\Entity
 * @ORM\Table(name="work_contracts_contracts")
 */
class Contract
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DONE = 'done';
    public const STATUS_TERMINATED = 'terminated';

    /**
     * @ORM\Column(type="work_contracts_contract_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $number;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(
     *  targetEntity="App\Model\Work\Entity\Contracts\Contract\Person\Person",
     *  inversedBy="contracts"
     * )
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     */
    private $person;

    /**
     * @ORM\Column(type="string", name="contract_sum")
     */
    private $sum;

    /**
     * @var Investigation
     *
     * @ORM\ManyToOne(
     *  targetEntity="App\Model\Work\Entity\Investigations\Investigation\Investigation",
     *  inversedBy="contracts"
     * )
     * @ORM\JoinColumn(
     *  name="investigation_id",
     *  referencedColumnName="id"
     * )
     */
    private $investigation;

    public function __construct(
        Id $id,
        string $number,
        \DateTimeImmutable $date,
        string $name,
        string $description,
        Person $person,
        string $sum
    ) {
        $this->id = $id;
        $this->number = $number;
        $this->date = $date;
        $this->name = $name;
        $this->description = $description;
        $this->person = $person;
        $this->sum = $sum;
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get the value of date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }
}
