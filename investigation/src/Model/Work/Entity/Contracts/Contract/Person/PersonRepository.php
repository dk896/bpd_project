<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Contracts\Contract\Person;

use App\Model\EntityNotFoundException;
use Doctrine\ORM\EntityManagerInterface;

class PersonRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Person::class);
    }

    /**
     * @param Id $id
     * @return Person
     */
    public function get(Id $id): Person
    {
        /** @var Person $person */
        if (!$person = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Person is not found.');
        }

        return $person;
    }

    /**
     * @param Email $email
     * @return Person
     */
    public function getByEmail(Email $email): Person
    {
        /** @var Person $person */
        if (!$person = $this->repo->findOneBy([
            'email' => $email->getValue(),
        ])) {
            throw new EntityNotFoundException('Person is not found.');
        }

        return $person;
    }

    /**
     * @param Email $email
     * @return boolean
     */
    public function hasByEmail(Email $email): bool
    {
        return $this->repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->andWhere('t.email = :email')
            ->setParameter(':email', $email->getValue())
            ->getQuery()
            ->getSingleScalarResult()
        > 0;
    }

    /**
     * @param Person $person
     * @return void
     */
    public function add(Person $person): void
    {
        $this->em->persist($person);
    }
}
