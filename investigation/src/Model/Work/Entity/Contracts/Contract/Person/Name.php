<?php

declare (strict_types = 1);

namespace App\Model\Work\Entity\Contracts\Contract\Person;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class Name
{
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $first;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $middle;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $last;

    public function __construct(string $first, string $middle, string $last)
    {
        Assert::notEmpty($first);
        Assert::notEmpty($middle);
        Assert::notEmpty($last);

        $this->first = $first;
        $this->middle = $middle;
        $this->last = $last;
    }

    /**
     * Get the value of first
     *
     * @return  string
     */
    public function getFirst(): string
    {
        return $this->first;
    }

    /**
     * Get the value of first
     *
     * @return  string
     */
    public function getMiddle(): string
    {
        return $this->middle;
    }

    /**
     * Get the value of last
     *
     * @return  string
     */
    public function getLast(): string
    {
        return $this->last;
    }

    public function getFull(): string
    {
        return $this->first
        . ' '
        . $this->middle
        . $this->last
        ;
    }
}
